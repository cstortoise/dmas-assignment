
function closeMessage(element) {
    $(element).closest('.message').fadeOut();
}

function deleteTweet(tweetId) {
    $.ajax({
        url: '/api/tweet/' + tweetId,
        type: 'DELETE',
        success: function (result) {
            console.log("deleted");
            location.reload();
        }
    });
}

function deleteMarkedTweets() {
    const removeCheckboxes = $(".bulk_remove");

    for (let i = 0; i < removeCheckboxes.length; i++) {
        if ($(removeCheckboxes[i]).is(':checked')) {
            $.ajax({
                url: '/api/tweet/' + removeCheckboxes[i].dataset.id,
                type: 'DELETE',
                success: function (result) {
                    console.log("deleted");
                }
            });
        }
    }

    location.reload();
}

function deleteAllTweets() {
    $.ajax({
        url: '/api/tweet/user/all',
        type: 'DELETE',
        success: function (result) {
            console.log("deleted");
        }
    });
}

function followUser(id) {
    $.ajax({
        url: '/api/user/follow',
        contentType: 'application/x-www-form-urlencoded',
        data: {'id': id},
        type: 'POST',
        success: function (result) {
            console.log(result);
        }
    });
}

function unfollowUser(id) {
    $.ajax({
        url: '/api/user/follow/' + id,
        type: 'DELETE',
        success: function (result) {
            console.log(result);
            location.reload();
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function deleteUser(userId) {
    $.ajax({
        url: '/api/user/' + userId,
        type: 'DELETE',
        success: function (result) {
            console.log("deleted");
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#info_area").prepend('<div class="ui negative floating message"><i class="close icon" onclick="closeMessage(this)"></i><div class="header">Failure</div><p>Failed to delete the user.</p></div>');
        }
    });
}

function createUser() {
    const newUser = {};
    newUser.name = $('#new_user_name').val();
    newUser.password = $('#new_user_password').val();
    newUser.email = $('#new_user_email').val();
    newUser.isAdmin = $('#new_user_admin').is(':checked');
    console.log("hi user" + newUser);

    $.ajax({
        url: '/api/user',
        type: 'POST',
        data: newUser,
        success: function (result) {
            console.log("added user" + result);
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error");
            $("#info_area").prepend('<div class="ui negative floating message"><i class="close icon" onclick="closeMessage(this)"></i><div class="header">Failure</div><p>Could not create the user. E-Mail already in use.</p></div>');
        }
    });
}

function updateUser(userId) {
    const userToUpdate = {};
    userToUpdate.name = $('#user_' + userId + '_name').val();
    userToUpdate.isAdmin = $('#user_' + userId + '_admin').is(':checked');
    userToUpdate.email = $('#user_' + userId + '_email').val();
    userToUpdate._id = $('#user_' + userId + '_id').html();
    console.log(userToUpdate);

    $.ajax({
        url: '/api/user/' + userId,
        type: 'PUT',
        data: userToUpdate,
        success: function (result) {
            $("#info_area").prepend('<div class="ui positive floating message"><i class="close icon" onclick="closeMessage(this)"></i><div class="header">Success</div><p>User updated.</p></div>');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#info_area").prepend('<div class="ui negative floating message"><i class="close icon" onclick="closeMessage(this)"></i><div class="header">Failure</div><p>User not updated.</p></div>');
        }
    });
}

$(document).ready(function () {
    $('.ui.menu')
            .on('click', '.item', function () {
                $(this)
                        .addClass('active')
                        .siblings('.item')
                        .removeClass('active');
            });
});