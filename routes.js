const Assets = require('./app/controllers/assets');
const Accounts = require('./app/controllers/accounts');
const Users = require('./app/controllers/users');
const Firehose = require('./app/controllers/firehose');

module.exports = [
    {method: 'GET',
        path: '/',
        config: Accounts.main},
    {method: 'POST',
        path: '/login',
        config: Accounts.authenticate},
    {method: 'GET',
        path: '/logout',
        config: Accounts.logout},
    {method: 'POST',
        path: '/register',
        config: Accounts.register},
    {method: 'GET',
        path: '/account',
        config: Accounts.viewAccount},
    {method: 'POST',
        path: '/account',
        config: Accounts.updateAccountDetails},
    {method: 'POST',
        path: '/tweet',
        config: Users.tweet},
    {method: 'GET',
        path: '/users',
        config: Users.viewAllUsers},
    {method: 'GET',
        path: '/user/{id}',
        config: Users.viewUser},
    {method: 'GET',
        path: '/follower',
        config: Users.follower},
    {method: 'GET',
        path: '/following',
        config: Users.following},
    {method: 'GET',
        path: '/firehose',
        config: Firehose.firehose},
    {
        method: 'GET',
        path: '/{param*}',
        config: {auth: false},
        handler: Assets.servePublicDirectory
    }
];