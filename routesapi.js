const TweetsApi = require('./app/api/tweetsapi');
const UserApi = require('./app/api/userapi');

module.exports = [
    {method: 'DELETE',
        path: '/api/tweet/{id}',
        config: TweetsApi.deleteOne},
    {method: 'DELETE',
        path: '/api/tweet/user/all',
        config: TweetsApi.deleteAllOfUser},
    {method: 'POST',
        path: '/api/user/follow',
        config: UserApi.followUser},
    {method: 'DELETE',
        path: '/api/user/follow/{id}',
        config: UserApi.unfollowUser},
    {method: 'DELETE',
        path: '/api/user/{id}',
        config: UserApi.deleteOne},
    {method: 'POST',
        path: '/api/user',
        config: UserApi.addUser},
    {method: 'PUT',
        path: '/api/user/{id}',
        config: UserApi.updateUser},
    {method: 'GET',
        path: '/api/tweet/all',
        config: TweetsApi.getAll}
];
