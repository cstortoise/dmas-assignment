module.exports = function (context) {
    if (context instanceof Buffer) {
        return context.toString('base64');
    } else {
        return context;
    }
};
