module.exports = function (context) {
    if (context instanceof Date) {
        return context.getFullYear();
    } else {
        return context;
    }
};
