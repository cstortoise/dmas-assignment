module.exports = function (v1, v2, options) {
    if (v1._id + "" === v2._id + "" || v2.isAdmin) {
        return options.fn(this);
    }

    return options.inverse(this);
};

