'use strict';

const User = require('../models/user');
const Tweet = require('../models/tweet');
const Joi = require('joi');
const fileType = require('file-type');
const ObjectId = require('mongoose').Types.ObjectId;

exports.viewAllUsers = {
    handler: function (request, reply) {
        const currentUserId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({_id: currentUserId}).then(currentUser => {
            User.find({
                _id: {$ne: currentUserId},
                isAdmin: false
            }).then(allUsers => {
                reply.view('users', {
                    currentUser: currentUser,
                    title: 'All users',
                    users: allUsers});
            }).catch(err => {
                reply.redirect('/');
            });
        }).catch(err => {
            reply.redirect('/');
        });
    }
};

exports.viewUser = {
    handler: function (request, reply) {
        const userId = new ObjectId(request.params.id);
        const currentUserId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({_id: currentUserId}).then(currentUser => {
            User.findOne({_id: userId}).then(foundUser => {
                Tweet.find({"user": foundUser}).populate("user").then(foundUserTweets => {
                    foundUser.tweetCount = foundUserTweets.length;

                    const alreadyFollowing = currentUser.following.find(function isFollowing(uId) {
                        return ("" + userId) === ("" + uId);
                    });

                    reply.view('timeline', {
                        currentUser: currentUser,
                        isFollower: alreadyFollowing === undefined ? false : true,
                        title: 'Not really Twitter | ' + foundUser.name,
                        user: foundUser,
                        tweets: foundUserTweets});
                }).catch(err => {
                    reply.redirect('/users');
                });
            }).catch(err => {
                reply.redirect('/users');
            });
        }).catch(err => {
            reply.redirect('/');
        });
    }
};

exports.timeline = {
    handler: function (request, reply) {
        Tweet.find({}).populate('user').then(allUserTweets => {
            reply.view('home', {
                title: 'Not really Twitter',
                tweets: allUserTweets
            });
        }).catch(err => {
            reply.redirect('/');
        });
    }
};

exports.tweet = {
    validate: {
        payload: {
            output: "stream",
            parse: true,
            allow: "multipart/form-data",
            maxBytes: 5 * 1000 * 1000,
            text: Joi.string().required().max(140),
            img: Joi.any()
        },

        failAction: function (request, reply, source, error) {
            reply.view('home', {
                title: 'Post error',
                errors: error.data.details
            }).code(400);
        },

        options: {
            abortEarly: false
        }
    },
    handler: function (request, reply) {
        const userId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({_id: userId}).then(currentUser => {
            const newTweet = new Tweet(request.payload);
            newTweet.date = new Date();

            if (request.payload.img !== undefined && request.payload.img !== null
                    && request.payload.img !== {} && request.payload.img.length
                    && request.payload.img.length !== undefined) {
                const ftype = fileType(request.payload.img);

                if (ftype.mime === "image/jpeg" || ftype.mime === "image/png") {
                    newTweet.img.data = request.payload.img;
                    newTweet.img.contentType = ftype.mime;
                }
            }

            newTweet.user = currentUser._id;
            newTweet.save().then(persistetTweet => {
                reply.redirect('/');
            }).catch(err => {
                console.log(err);
                reply.redirect('/');
            });
        }).catch(err => {
            console.log(err);
            reply.redirect('/');
        });
    }
};

exports.follower = {
    handler: function (request, reply) {
        const userId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({_id: userId}).populate({
            path: 'follower',
            model: 'User'
        }).then(currentUser => {
            reply.view('follower', {
                currentUser: currentUser,
                follower: currentUser.follower,
                title: 'Not really Twitter | ' + currentUser.name
            });
        }).catch(err => {
            reply.redirect('/');
        });
    }
};


exports.following = {
    handler: function (request, reply) {
        const userId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({_id: userId}).populate({
            path: 'following',
            model: 'User'
        }).then(currentUser => {
            reply.view('following', {
                currentUser: currentUser,
                following: currentUser.following,
                title: 'Not really Twitter | ' + currentUser.name
            });
        }).catch(err => {
            reply.redirect('/');
        });
    }
};
