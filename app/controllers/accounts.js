'use strict';

const User = require('../models/user');
const Joi = require('joi');
const Tweet = require('../models/tweet');
const ObjectId = require('mongoose').Types.ObjectId;

exports.main = {
    auth: {mode: 'try'},
    plugins: {
        'hapi-auth-cookie': {
            redirectTo: false
        }
    },
    handler: function (request, reply) {
        if (request.auth.isAuthenticated === true) {
            const userId = new ObjectId(request.auth.credentials.loggedInUserId);

            User.findOne({_id: userId}).then(currentUser => {
                if (currentUser.isAdmin) {
                    Tweet.find({}).then(allTweets => {
                        User.find({}).then(allUsers => {
                            reply.view('home', {
                                currentUser: currentUser,
                                title: 'Not really Twitter | ' + currentUser.name,
                                users: allUsers,
                                tweetCount: allTweets.length
                            });
                        });
                    }).catch(err => {
                        reply.view('main', {title: 'Not Really Twitter | Welcome'});
                    });
                } else {
                    Tweet.find({'user': {"$in": currentUser.following.concat(currentUser)}}).sort('-date').populate('user').then(allUserTweets => {
                        let tweetCount = 0;

                        for (let i = 0; i < allUserTweets.length; i++) {
                            if (allUserTweets[i].user._id + "" === currentUser._id + "") {
                                tweetCount++;
                            }
                        }

                        currentUser.tweetCount = tweetCount;

                        reply.view('home', {
                            currentUser: currentUser,
                            title: 'Not really Twitter | ' + currentUser.name,
                            tweets: allUserTweets
                        });
                    }).catch(err => {
                        reply.view('main', {title: 'Not Really Twitter | Welcome'});
                    });
                }
            }).catch(err => {
                reply.view('main', {title: 'Not Really Twitter | Welcome'});
            });
        } else {
            reply.view('main', {title: 'Not Really Twitter | Welcome'});
        }
    }
};

exports.authenticate = {
    auth: false,
    validate: {
        payload: {
            email: Joi.string().email().required(),
            password: Joi.string().required()
        },

        failAction: function (request, reply, source, error) {
            reply.view('main', {
                title: 'Login error',
                errors: error.data.details
            }).code(400);
        },

        options: {
            abortEarly: false
        }
    },
    handler: function (request, reply) {
        const user = request.payload;
        User.findOne({email: user.email}).then(foundUser => {
            if (foundUser && foundUser.password === user.password) {
                request.cookieAuth.set({
                    loggedIn: true,
                    loggedInUserId: foundUser._id
                });
                reply.redirect('/');
            } else {
                reply.redirect('/');
            }
        }).catch(err => {
            reply.redirect('/');
        });
    }
};

exports.logout = {
    auth: false,
    handler: function (request, reply) {
        request.cookieAuth.clear();
        reply.redirect('/');
    }
};

exports.register = {
    auth: false,
    validate: {
        payload: {
            name: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().required()
        },

        failAction: function (request, reply, source, error) {
            reply.view('main', {
                title: 'Sign up error',
                signuperrors: error.data.details
            }).code(400);
        },

        options: {
            abortEarly: false
        }
    },
    handler: function (request, reply) {
        const user = new User(request.payload);
        User.findOne({email: user.email}).then(foundUser => {
            if (foundUser === null) {
                user.date = new Date();
                user.isAdmin = false;

                user.save().then(newUser => {
                    request.cookieAuth.set({
                        loggedIn: true,
                        loggedInUserId: newUser._id
                    });
                    reply.redirect('/');
                }).catch(err => {
                    reply.redirect('/');
                });
            } else {
                reply.view('main', {
                    title: 'Sign up error',
                    signuperrors: [{
                            message: 'E-Mail already in use',
                            path: 'email'}]
                }).code(400);
            }
        }).catch(err => {
            reply.redirect('/');
        });
    }
};

exports.viewAccount = {
    handler: function (request, reply) {
        const userId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({_id: userId}).then(currentUser => {
            reply.view('account', {
                title: 'Not Really Twitter | Account Details',
                currentUser: currentUser});
        }).catch(err => {
            reply.redirect('/');
        });
    }
};

exports.updateAccountDetails = {
    validate: {
        payload: {
            name: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().required()
        },

        failAction: function (request, reply, source, error) {
            reply.view('account', {
                title: 'Not Really Twitter | Settings error',
                errors: error.data.details
            }).code(400);
        },

        options: {
            abortEarly: false
        }
    },
    handler: function (request, reply) {
        const modifiedUser = request.payload;
        const userId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({email: modifiedUser.email}).then(foundUser => {
            if (foundUser === null || foundUser.email === modifiedUser.email) {
                User.update({_id: userId}, {
                    email: modifiedUser.email,
                    name: modifiedUser.name,
                    password: modifiedUser.password
                }, function (err, numberAffected, rawResponse) {
                    if (err) {
                        reply.redirect('/');
                    } else {
                        reply.redirect('/account');
                    }
                });
            } else {
                User.findOne({_id: userId}).then(currentUser => {
                    reply.view('account', {
                        title: 'Not Really Twitter | Settings error',
                        user: currentUser,
                        errors: [{
                                message: 'E-Mail already in use',
                                path: 'email'}]
                    }).code(400);
                }).catch(err => {
                    reply.redirect('/account');
                });
            }
        }).catch(err => {
            reply.redirect('/account');
        });
    }
};
