'use strict';

const User = require('../models/user');
const Tweet = require('../models/tweet');
const ObjectId = require('mongoose').Types.ObjectId;

exports.firehose = {
    handler: function (request, reply) {
        const currentUserId = new ObjectId(request.auth.credentials.loggedInUserId);

        User.findOne({_id: currentUserId}).then(currentUser => {
            Tweet.find({}).sort('-date').populate('user').then(allTweets => {
                reply.view('firehose', {
                    title: 'Not really Twitter | Firehose',
                    currentUser: currentUser,
                    tweets: allTweets});
            }).catch(err => {
                reply.redirect('/');
            });
        }).catch(err => {
            reply.redirect('/');
        });
    }
};
