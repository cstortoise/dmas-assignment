'use strict';

const User = require('../models/user');
const Tweet = require('../models/tweet');
const ObjectId = require('mongoose').Types.ObjectId;
const Boom = require('boom');

exports.followUser = {
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            Boom.unauthorized('Insufficient permission');
        }

        const userId = new ObjectId(request.payload.id);
        const currentUserId = new ObjectId(request.state.sessioncookie.loggedInUserId);

        User.findOne({_id: currentUserId}).then(function (currentUser) {
            if (currentUser === undefined) {
                Boom.notFound('User not found.');
            }

            const alreadyFollowing = currentUser.following.find(function isFollowing(user) {
                return user._id == request.payload.id;
            });

            if (alreadyFollowing === undefined) {
                User.findById(userId, function (err, userToFollow) {
                    if (err) {
                        Boom.notFound('User not found.');
                    }

                    User.update({_id: new ObjectId(currentUser._id)}, {
                        $push: {following: userToFollow}
                    }, function (err, numberAffected, rawResponse) {
                        if (err) {
                            reply('Error').code(500);
                        } else {
                            User.update({_id: userId}, {
                                $push: {follower: currentUser}
                            }, function (err, numberAffected, rawResponse) {
                                if (err) {
                                    reply('Error').code(500);
                                } else {
                                    reply('Success').code(200);
                                }
                            });
                        }
                    });
                });
            } else {
                reply('Already following').code(409);
            }
        }).catch(err => {
            Boom.forbidden('Insufficient permission');
        });
    }
};

exports.unfollowUser = {
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            Boom.unauthorized('Insufficient permission');
        }

        const userId = new ObjectId(request.params.id);
        const currentUserId = new ObjectId(request.state.sessioncookie.loggedInUserId);

        User.findOne({_id: currentUserId}).then(function (currentUser) {
            if (currentUser === undefined) {
                reply('User not found.').code(404);
            }

            const isFollowing = currentUser.following.find(function isFollowing(uId) {
                return ("" + uId) === (userId + "");
            });

            if (isFollowing !== undefined) {
                User.findById(userId, function (err, userToFollow) {
                    if (err) {
                        reply('User not found.').code(404);
                    }

                    User.update({_id: currentUser._id}, {
                        $pull: {following: userToFollow._id}
                    }, function (err, numberAffected, rawResponse) {
                        if (err) {
                            reply('Error').code(500);
                        } else {
                            User.update({_id: userToFollow._id}, {
                                $pull: {follower: currentUser._id}
                            }, function (err, numberAffected, rawResponse) {
                                if (err) {
                                    reply('Error').code(500);
                                } else {
                                    reply('Success').code(200);
                                }
                            });
                        }
                    });
                });
            } else {
                reply('Not following.').code(200);
            }
        }).catch(err => {
            Boom.forbidden('Insufficient permission');
        });
    }
};

exports.deleteOne = {
    auth: false,
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            Boom.unauthorized('Insufficient permission');
        }

        const userId = new ObjectId(request.state.sessioncookie.loggedInUserId);

        User.findOne({_id: userId}).then(currentUser => {
            if (currentUser.isAdmin === true) {
                User.update({}, {$pull: {follower: currentUser._id}});
                User.update({}, {$pull: {following: currentUser._id}});
                Tweet.remove({user: currentUser._id});
                User.remove({_id: request.params.id}).then(user => {
                    reply(user).code(204);
                }).catch(err => {
                    Boom.notFound('id not found');
                });
            } else {
                Boom.forbidden('Insufficient permission');
            }
        }).catch(err => {
            Boom.forbidden('Insufficient permission');
        });
    }
};

exports.addUser = {
    auth: false,
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            Boom.unauthorized('Insufficient permission');
        }

        const userId = new ObjectId(request.state.sessioncookie.loggedInUserId);

        User.findOne({_id: userId}).then(currentUser => {
            if (currentUser.isAdmin === true) {
                const user = new User(request.payload);
                User.findOne({email: user.email}).then(foundUser => {
                    if (foundUser === null) {
                        user.date = new Date();

                        user.save().then(newUser => {
                            reply(newUser).code(204);
                        }).catch(err => {
                            reply('Error').code(400);
                        });
                    }
                }).catch(err => {
                    Boom.conflict('E-Mail already in use');
                });
            } else {
                Boom.forbidden('Insufficient permission');
            }
        }).catch(err => {
            Boom.forbidden('Insufficient permission');
        });
    }
};

exports.updateUser = {
    auth: false,
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            Boom.unauthorized('Insufficient permission');
        }

        const modifiedUser = request.payload;
        const userId = new ObjectId(request.state.sessioncookie.loggedInUserId);

        User.findOne({_id: userId}).then(foundUser => {
            if (foundUser.isAdmin) {
                User.update({_id: modifiedUser._id}, {
                    email: modifiedUser.email,
                    name: modifiedUser.name,
                    isAdmin: modifiedUser.isAdmin
                }, function (err, numberAffected, rawResponse) {
                    if (err) {
                        reply('Error').code(400);
                    } else {
                        reply().code(204);
                    }
                });
            } else {
                Boom.notFound('User not found');
            }
        }).catch(err => {
            Boom.forbidden('Insufficient permission');
        });
    }
};
