'use strict';

const Tweet = require('../models/tweet');
const User = require('../models/user');
const Boom = require('boom');
const ObjectId = require('mongoose').Types.ObjectId;

exports.deleteOne = {
    auth: false,
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            reply('Insufficient permission').code(401);
        }

        const userId = new ObjectId(request.state.sessioncookie.loggedInUserId);

        User.findOne({_id: userId}).then(currentUser => {
            Tweet.findOne({_id: request.params.id}).then(tweet => {
                if (currentUser.isAdmin || tweet.user + "" === currentUser._id + "") {
                    Tweet.remove({_id: request.params.id}).then(tweet => {
                        reply(tweet).code(204);
                    }).catch(err => {
                        reply(Boom.notFound('id not found'));
                    });
                } else {
                    reply('Insufficient permission').code(403);
                }
            }).catch(err => {
                reply('Insufficient permission').code(403);
            });
        }).catch(err => {
            reply('Insufficient permission').code(403);
        });
    }
};

exports.getAll = {
    auth: false,
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            reply('Insufficient permission').code(401);
        }

        Tweet.find({}).then(allTweets => {
            reply(allTweets).code(204);
        }).catch(err => {
            reply('Insufficient permission').code(403);
        });
    }
};

exports.deleteAllOfUser = {
    auth: false,
    handler: function (request, reply) {
        if (request.state.sessioncookie.loggedIn === false) {
            reply('Insufficient permission').code(401);
        }

        const userId = new ObjectId(request.state.sessioncookie.loggedInUserId);

        User.findOne({_id: userId}).then(currentUser => {
            Tweet.remove({'user': currentUser}).then(tweet => {
                reply(tweet).code(204);
            }).catch(err => {
                reply(Boom.notFound('id not found'));
            });
        }).catch(err => {
            reply('Insufficient permission').code(403);
        });
    }
};
