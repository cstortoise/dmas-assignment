'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    text: String,
    date: Date,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    img: {
        data: Buffer,
        contentType: String
    }
});

const Tweet = mongoose.model('Tweet', userSchema);
module.exports = Tweet;
