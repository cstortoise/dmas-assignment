'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: String,
    email: String,
    password: String,
    date: Date,
    follower: [{type: mongoose.Schema.Types.ObjectId,
            ref: this}],
    following: [{type: mongoose.Schema.Types.ObjectId,
            ref: this}],
    isAdmin: Boolean
});

const User = mongoose.model('User', userSchema);
module.exports = User;
