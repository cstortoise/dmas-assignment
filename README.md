# DMAS OTH Assignment #
### Author ###

Name: Christian Sch�tze
Student-ID: 3010299

### Project Description ###

The App is a simplified Twitter clone, which allows it's users to share short messages/tweets to each other. It's possible to follow other users and to view their tweets.

### Deployment ###

* The App is deployed on Heroku
* The App is using MLab for Mongo DB hosting

### Links ###

* Deployed Web App: https://calm-beach-47848.herokuapp.com/
* Video Link: https://youtu.be/GEtbS_CQOuQ
* Repository: https://bitbucket.org/SchChr/dmas-assignment
