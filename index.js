'use strict';

require('./app/models/db');
const Hapi = require('hapi');
const Handlebars = require('handlebars');
const HandlebarsIntl = require('handlebars-intl');
HandlebarsIntl.registerWith(Handlebars);

const server = new Hapi.Server();
server.connection({port: process.env.PORT || 4000});

server.register([require('inert'), require('vision'), require('hapi-auth-cookie')], err => {

    if (err) {
        throw err;
    }

    server.views({
        engines: {
            hbs: Handlebars
        },
        relativeTo: __dirname,
        path: './app/views',
        layoutPath: './app/views/layout',
        partialsPath: './app/views/partials',
        layout: true,
        isCached: false,
        helpersPath: './app/helpers'
    });

    server.auth.strategy('standard', 'cookie', {
        password: 'secretpasswordnotrevealedtoanyone',
        cookie: 'sessioncookie',
        isSecure: false,
        ttl: 24 * 60 * 60 * 1000,
        redirectTo: '/login'
    });

    server.auth.default({
        strategy: 'standard'
    });

    server.route(require('./routes'));
    server.route(require('./routesapi'));

    server.start((err) => {
        if (err) {
            throw err;
        }

        console.log('Server listening at:', server.info.uri);
    });
});
